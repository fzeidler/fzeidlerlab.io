import os
from PIL import Image

def resize_and_convert(input_path, output_dir, sizes):
    filename = os.path.basename(input_path)
    name, ext = os.path.splitext(filename)
    
    img = Image.open(input_path)
    
    # Convert RGBA images to RGB
    if img.mode == 'RGBA':
        img = img.convert('RGB')
    
    for size in sizes:
        # Resize image
        resized_img = img.copy()
        resized_img.thumbnail((size, size))
        
        # Save as WebP
        output_filename = f"{name}_{size}w.webp"
        output_path = os.path.join(output_dir, output_filename)
        resized_img.save(output_path, 'WEBP', quality=90)  # Adjust quality as needed
        
        print(f"Saved {output_filename}")

def process_images(input_dir, output_dir, sizes):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    for filename in os.listdir(input_dir):
        if filename.lower().endswith(('.png', '.jpg', '.jpeg', '.webp')):
            input_path = os.path.join(input_dir, filename)
            resize_and_convert(input_path, output_dir, sizes)

# Define sizes based on breakpoints in style.css
sizes = [940, 1920, 3840]  # Adjust these values based on your needs

# Set your input and output directories
input_dir = 'img'
output_dir = 'img/resized'

process_images(input_dir, output_dir, sizes)
