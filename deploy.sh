#!/bin/bash

# Colors for output
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Remote server details
REMOTE_HOST="uberspace7"
REMOTE_PATH="html"  # Assuming standard uberspace setup with 'html' directory

echo "🚀 Starting deployment to ${REMOTE_HOST}..."

# Check if we can connect to the server
if ! ssh -q ${REMOTE_HOST} exit; then
  echo -e "${RED}Error: Cannot connect to ${REMOTE_HOST}${NC}"
  exit 1
fi

# Sync files using rsync
# -a: archive mode (recursive, preserve permissions, etc.)
# -v: verbose
# -z: compress during transfer
# --delete: remove files on remote that don't exist locally
# --exclude: skip version control and system files
rsync -avz --delete \
  --exclude '.git/' \
  --exclude '.DS_Store' \
  --exclude 'node_modules/' \
  --exclude 'deploy.sh' \
  --exclude '*.py' \
  ./*.html \
  ./*.css \
  ./img \
  ${REMOTE_HOST}:~/${REMOTE_PATH}/

if [ $? -eq 0 ]; then
  echo -e "${GREEN}✨ Deployment successful!${NC}"
else
  echo -e "${RED}❌ Deployment failed${NC}"
  exit 1
fi 